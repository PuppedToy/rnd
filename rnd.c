#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>

#include "rnd.h"

int hidden = 0, extension_enabled = 1;
char my_dir[256];

int main(int argc, char * argv[]) {

	bzero(my_dir, sizeof(my_dir));

	strcpy(my_dir, ".");

	int len_n = LEN_NAMES, i, j, backup = 1, undo_chk = 0;
	
	for(i = 1; i < argc; i++) {
		if(strcmp(argv[i], "-l") == 0) {
			if(argc >= i+2) len_n = atoi(argv[++i]);

			else {
				fprintf(stderr, NAME": -l option must have a value.\n");
				exit(1);
			}
		}
		else if(argv[i][0] == '-') {
			for(j = 1; j < strlen(argv[i]); j++) {
				switch(argv[i][j]) {
					case 'a':
						hidden = 1;
						break;
					case 'e':
						extension_enabled = 0;
						break;
					case 'b':
						backup = 0;
						break;
					case 'u':
						undo_chk = 1;
						break;
					case 'h':
						printf(NAME": Change the name of every regular file in working directory or [PATH] to random names.\n\n");
						printf(NAME": Usage: rnd [PATH] [OPTIONS]\n\n");
						printf(NAME": Valid options:\n");
						printf("\t-a: make every file hidden (name starting by '.')\n");
						printf("\t-b: disable backup file creating, negating the possibility to undo.\n");
						printf("\t-e: disable extension protection (extension will change with the name too)\n");
						printf("\t-h: display the help and return\n");
						printf("\t-l VALUE: change the length of the names (by default %d) to VALUE\n", LEN_NAMES);
						printf("\t-u: undo last rnd command in the directory.\n");
						printf("\n");
						exit(0);
					case 'l':
						fprintf(stderr, NAME": fatal error: Option -%c can't be stacked with other options.\n", argv[i][j]);
						exit(1);
					default:
						fprintf(stderr, NAME": fatal error: Option not found: -%c.\n", argv[i][j]);
						exit(1);
				}
			}
		}
		else strcpy(my_dir, argv[i]);
	}
	if(len_n <= 0) {
		fprintf(stderr, NAME": fatal error: The length of the names can't be less than 1\n");
		exit(1);
	}
	if(len_n >= 256) {
		fprintf(stderr, NAME": fatal error: The length of the names can't be more than 255\n");
		exit(1);
	}
	if(undo_chk) undo();
	i = 0;

	DIR * dir;
	if((dir = opendir(my_dir)) == NULL) {
		perror(NAME": Open directory");
	}
	struct dirent * ent;
	struct file files[MAX_FILES];

	while((ent = readdir(dir)) != NULL) {
		if(ent->d_type == DT_REG && strstr(ent->d_name, ".undo.rnd") != ent->d_name) files[i++] = new_file(ent->d_name, len_n);
	}

	int undofd;
	char buf[MAX_FILES];
	sprintf(buf, "%s/.undo.rnd", my_dir);
	if(backup && (undofd = open(buf, O_RDWR | O_CREAT | O_TRUNC, 0644)) < 0) {
		perror(NAME": WARNING: Can't create backup file. Continue? [Y/N]");
		read(0, buf, 1);
		if(*buf != 'y' && *buf != 'Y') {
			fprintf(stderr, NAME": Aborted operation\n");
			close(undofd);
			exit(1);
		}
	}

	for(j = 0; j < i; j++) {
		if(extension_enabled) sprintf(buf, "mv '%s/%s' '%s/%s%s'", my_dir, files[j].old, my_dir, files[j].new, files[j].ext);
		else sprintf(buf, "mv '%s/%s' '%s/%s'", my_dir, files[j].old, my_dir, files[j].new);
		if(system(buf) < 0) perror(NAME": System call");
		else if (backup) {
			write(undofd, files[j].old, strlen(files[j].old));
			write(undofd, "\t", 1);
			if(extension_enabled) {
				sprintf(buf, "%s%s", files[j].new, files[j].ext);
				write(undofd, buf, strlen(buf));
			}
			else write(undofd, files[j].new, strlen(files[j].new));
			write(undofd, "\n", 1);
		}
	}

	if(backup) close(undofd);

	return 0;

}

struct file new_file(char * name, int len_n) {
	struct file res;
	bzero((char *) &res, sizeof(res));
	strcpy(res.old, name);
	int ext_penalty = 0;
	if(extension_enabled) ext_penalty = extension(res.ext, name);
	random_name(res.new, len_n - ext_penalty);
	return res;
}

void random_name(char * buf, int len_n) {
	int i = 0;
	if(hidden) buf[i++] = '.';
	buf[i++] = random_char_first();
	for(; i < len_n; i++) {
		buf[i] = random_char();
	}
}

char random_char() {
	int fd = open("/dev/urandom", O_RDONLY);
	char res;
	do {
		read(fd, &res, 1);
	} while(res <= 32 || res >= 127 || res == 34 || res == 39 || res == 47 || res == 92);
	close(fd);
	return res;
}

char random_char_first() {
	char res;
	do {
		res = random_char();
	} while(res < 48 || (res > 57 && res < 65) || (res > 90 && res < 97) || res > 122);
	return res;
}

int extension(char * ext, char * name) {
	int i;
	for(i = strlen(name)-1; i >= 0; i--) {
		if(name[i] == '.') break;
	}
	strcpy(ext, name+i);
	return strlen(ext);
}

void undo() {
	int undofd;
	char buf[MAX_FILES];
	sprintf(buf, "%s/.undo.rnd", my_dir);
	if((undofd = open(buf, O_RDWR)) < 0) {
		perror(NAME": fatal error: undo file doesn't exist.");
		exit(1);
	}
	
	struct file f = read_line(undofd);
	while(f.new[0] != 127 && f.old[0] != 127) {
		sprintf(buf, "mv '%s/%s' '%s/%s'", my_dir, f.old, my_dir, f.new);
		if(system(buf) < 0) perror(NAME": System call");
		f = read_line(undofd);
	}
	if(f.old[0] == 127) {
		fprintf(stderr, NAME": fatal error: Backup file corrupted and can't be eliminated.\n");
		exit(1);
	}

	close(undofd);
	sprintf(buf, "%s/.undo.rnd", my_dir);
	if(unlink(buf) < 0) printf(NAME": Successful undo. Backup file eliminated.\n");
	else {
		perror(NAME": Eliminating backup file");
		printf(NAME": Successful undo.\n");
	}
	exit(0);
}

struct file read_line(int fd) {
	
	struct file f;
	char last, * buffer = f.new;
	int i = 0;

	do {
		switch(read(fd, &last, 1)) {
			case -1:
				perror(NAME": Reading backup file");
				exit(1);
			case 0:
				f.new[0] = 127;
				return f;
		}
		if(last == '\t') {
			buffer[i] = 0;
			buffer = f.old;
			i = 0;
		}
		else if(last != '\n') buffer[i++] = last;
	} while(i < 256 && last != '\n');
	if(i < 256) buffer[i] = 0;
	else f.old[0] = 127;

	return f;
}

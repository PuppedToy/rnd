#ifndef __RND_H__
#define __RND_H__

struct file {
	char old[256];
	char new[256];
	char ext[256];
};

#define MAX_FILES	2048
#define LEN_NAMES	64
#define NAME		"rnd"

struct file new_file(char * name, int len_n);
void random_name(char * buf, int len_n);
char random_char();
char random_char_first();
int extension(char * ext, char * name);
void undo();
struct file read_line(int fd);

#endif
